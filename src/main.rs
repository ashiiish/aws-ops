#[macro_use] extern crate prettytable;

extern crate rusoto_core;
extern crate rusoto_s3;
extern crate rusoto_emr;

mod aws;

use aws::emr;

fn main() {
    emr::ls();
    emr::cluster_info(String::from("j-308QR28XMM3MV"));
}
