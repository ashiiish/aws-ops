use rusoto_core::region::Region;
use rusoto_emr::{DescribeClusterInput, Emr, EmrClient, ListClustersInput};

use prettytable::format;
use prettytable::Table;

pub fn ls() {
    let client = EmrClient::new(Region::UsEast1);

    // let list_clusters_input: ListClustersInput = Default::default();
    let list_clusters_input: ListClustersInput = ListClustersInput {
        cluster_states: Some(vec![
            String::from("RUNNING"),
            String::from("STARTING"),
            String::from("BOOTSTRAPPING"),
            String::from("WAITING"),
            String::from("TERMINATING"),
        ]),
        created_after: None,
        created_before: None,
        marker: None,
    };

    match client.list_clusters(list_clusters_input).sync() {
        Ok(output) => match output.clusters {
            Some(clusters_list) => {
                let mut table = Table::new();
                table.set_titles(row![
                    "Cluster ID",
                    "Cluster Name",
                    "Status",
                    "Instance Hours"
                ]);
                for cluster in clusters_list {
                    table.add_row(row![
                        cluster.id.unwrap(),
                        cluster.name.unwrap(),
                        cluster.status.unwrap().state.unwrap(),
                        cluster.normalized_instance_hours.unwrap()
                    ]);
                }
                table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);
                table.printstd();
            }
            None => println!("No clusters running."),
        },
        Err(_error) => println!("Error retrieving clusters: {}", _error),
    }
}

pub fn cluster_info(cluster_id: String) {
    let client = EmrClient::new(Region::UsEast1);
    let cluster_info_input = DescribeClusterInput { cluster_id };

    match client.describe_cluster(cluster_info_input).sync() {
        Ok(output) => {
            match output.cluster {
                Some(cluster) => {
                    println!("Name: {} [{}]\nStatus: {}\nMaster Public DNS: {}\nRelease Label: {}\nLog URI: {}\nService Role: {}\nSecurity Configuration: {}",
                             cluster.name.unwrap_or(String::from("")),
                             cluster.id.unwrap_or(String::from("")),
                             cluster.status.unwrap().state.unwrap(),
                             cluster.master_public_dns_name.unwrap_or(String::from("")),
                             cluster.release_label.unwrap_or(String::from("")),
                             cluster.log_uri.unwrap_or(String::from("")),
                             cluster.service_role.unwrap_or(String::from("")),
                             cluster.security_configuration.unwrap_or(String::from("")))
                },
                None => println!("Cluster not found."),
            }
        },
        Err(_error) => println!("Error describing cluter: {}", _error),
    }
}

pub fn ls_steps(cluster_id: String) {
    // list_steps
    //  ListStepsInput { cluster_id, marker, step_ids, step_states }
}
